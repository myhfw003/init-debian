# 初始化Debian

```sh
#!/bin/bash

# 创建 lang.sh 文件
cat << 'EOF' > /etc/profile.d/lang.sh
export LC_ALL=C.utf8
EOF

# 创建 ls.sh 文件
cat << 'EOF' > /etc/profile.d/ls.sh
export LS_OPTIONS='--color=auto'
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'
EOF

echo "脚本文件已创建并放置在 /etc/profile.d 目录下。"

```

可以如下命令下载、执行

```sh
curl -O https://gitee.com/myhfw003/init-debian/raw/master/init_debian.sh


以下代码可以直接执行脚本，但不加检查的直接执行网络上的脚本是有较高安全风险的，请谨慎使用

curl -s https://gitee.com/myhfw003/init-debian/raw/master/init_debian.sh | bash

```