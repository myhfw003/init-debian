#!/bin/bash

# 创建 lang.sh 文件
cat << 'EOF' > /etc/profile.d/lang.sh
export LC_ALL=C.utf8
EOF

# 创建 ls.sh 文件
cat << 'EOF' > /etc/profile.d/ls.sh
export LS_OPTIONS='--color=auto'
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'
EOF

echo "脚本文件已创建并放置在 /etc/profile.d 目录下。"
